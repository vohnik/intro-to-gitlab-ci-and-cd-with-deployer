# Intro to Gitlab CI and CD with deployer

## References
- [Deployer](https://deployer.org)
- [DPL](https://github.com/travis-ci/dpl)
- [Gitlab CI](https://about.gitlab.com/product/continuous-integration/)
    - [Leading CI platform](https://about.gitlab.com/2017/09/27/gitlab-leader-continuous-integration-forrester-wave/)
    - [Gitlab CI for Github](https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/github_integration.html)
